module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            productionHtml: {
                expand: true,
                cwd: 'source/client',
                src: '*.html',
                dest: 'release/client/'
            },
            productionJS: {
                expand: true,
                cwd: 'source/client',
                src: '*.js',
                dest: 'release/client/'
            },
            productionCSS: {
                expand: true,
                cwd: 'source/client',
                src: '**/*.less',
                dest: 'release/client/'
            }
        },
        uglify: {
            productionJS: {
                files: {
                    'release/client/main.min.js': ['release/client/main.js']
                }
            }
        },
        less: {
            productionCSS: {
                files: {
                    'release/client/css/main.css': 'release/client/css/main.less'
                }
            },
            devCSS: {
                files: {
                    'source/client/css/main.css': 'source/client/css/main.less'
                }
            }
        },
        cssmin: {
            productionCSS: {
                options: {
                    mergeIntoShortHands: true
                },
                files: {
                    'release/client/css/main.min.css': ['release/client/css/main.css']
                }
            },
            devCSS: {
                options: {
                    mergeIntoShortHands: true
                },
                files: {
                    'source/client/css/main.min.css': ['source/client/css/main.css']
                }
            }
        },
        imagemin: {
            dynamic: {                         // Another target 
                files: [{
                    expand: true,                  // Enable dynamic expansion 
                    cwd: 'source/client/images',                   // Src matches are relative to this path 
                    src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match 
                    dest: 'release/client/images'                  // Destination path prefix 
                }]
            }
        }

    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    // Default task(s).
    grunt.registerTask('default', ['copy', 'uglify', 'less', 'cssmin', 'imagemin']);

};